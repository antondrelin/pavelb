#!/bin/sh

dir_base=/opt/tools_sync

# include script config 
source "$dir_base/config.sh"

# create struct folder file
function mkdir_folder() {	
   for item in ${file_folder[*]}
   do
	if [ ! -d "$item" ]; then
 	   mkdir "$item"	
	fi	
   done	   
}

#send message
send_message(){
	for item in ${email_send[*]} 
        do
	  echo $1 | mail -s $2 $item
	done     
}

# create archive
function create_archive_light() {  
   name_file="${data}_light".tar.gz    
   tar -zcf - "${home_bitrix}" --exclude-from="$dir_base/ext.txt" | pv -L 1024m -p > "$dir_base/archive/$name_file" 
}

function create_archive_full(){
   name_file="${data}_full".tar.gz
   tar -zcf  - "${home_bitrix}"  | pv -L 1024m  -p > "$dir_base/archive/$name_file"
}

function create_mysql(){	
   name_file="mysql_dump_${data}_dump.sql.gz"
   mysqldump -u ${db_config[2]} -p${db_config[3]} ${db_config[4]} | gzip | pv -L 1024m -p > "$dir_base/archive/$name_file"
}

function create_end_tar(){
   name="${data}_end.tar.gz"
   tar -zcf  - "$dir_base/archive/" | pv -L 1024m -p > "$dir_base/end/$name"
   mv "$dir_base/end/$name" "${home_bitrix}/tmp/$name"   
   md5sum "${home_bitrix}/tmp/$name" > "${home_bitrix}/tmp/one.txt"
}

mkdir_folder

echo "${data}"
rm -f "$dir_base/archive"/*.gz
rm -f "${home_bitrix}/tmp"/*.gz
rm -f "$dir_base/end/*"

echo "archive"
ls -lah "$dir_base/archive/"

echo "clear.."
create_mysql

if [ "$1" = "l" ]; then
   echo "light"
   create_archive_light   
   ls -lah "$dir_base/archive/"
fi
if [ "$1" = "f" ]; then
   echo "full"
   create_archive_full      
   ls -lah "$dir_base/archive/"
fi   

echo "end tar"
create_end_tar
ls -lah "${home_bitrix}/tmp/"